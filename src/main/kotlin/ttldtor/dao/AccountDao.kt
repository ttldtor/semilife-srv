package ttldtor.dao

import ttldtor.ConnectionPool
import ttldtor.entities.Account
import ttldtor.use
import ttldtor.useWithTx
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Statement

operator fun Account.Companion.invoke(c: Connection, rs: ResultSet): Account {
    return Account(
            id = rs.getLong("id"),
            login = rs.getString("login"),
            passwordHash = rs.getString("password_hash"),
            createDate = rs.getLong("create_date"),
            players = arrayListOf(),
            isDeleted = false).apply { players.addAll(PlayerDao.getByAccount(c, this)) }
}

object AccountDao {
    val CREATE_SQL = "INSERT INTO account (login, password_hash, create_date, is_deleted) VALUES (?, ?, ?, 0)"
    val SAVE_SQL = "UPDATE account SET login = ?, password_hash = ? WHERE id = ?"
    val DELETE_SQL = "UPDATE account SET is_deleted = 1 WHERE id = ?"
    val GET_BY_ID_SQL = "SELECT * FROM account WHERE is_deleted = 0 AND id = ?"
    val GET_SQL = "SELECT * FROM account WHERE is_deleted = 0"

    fun create(login: String, passwordHash: String, createDate: Long): Account? {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(CREATE_SQL, Statement.RETURN_GENERATED_KEYS) use st@ { st ->
                st.setString(1, login)
                st.setString(2, passwordHash)
                st.setLong(3, createDate)

                if (st.executeUpdate() == 0) {
                    return@st null
                }

                st.generatedKeys use keys@ { keys ->
                    if (keys.next()) {
                        return@keys Account(keys.getLong(1), login, passwordHash, createDate, arrayListOf())
                    }

                    null
                }
            }
        }
    }

    fun save(a: Account): Boolean {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(SAVE_SQL) use { st ->
                st.setString(1, a.login)
                st.setString(2, a.passwordHash)
                st.setLong(3, a.id)

                st.executeUpdate() == 1
            }
        }
    }

    fun delete(a: Account): Boolean {
        if (a.isDeleted) {
            return true
        }

        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(DELETE_SQL) use { st ->
                st.setLong(1, a.id)

                st.executeUpdate() == 1
            }
        }
    }

    fun getById(id: Long): Account? {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(GET_BY_ID_SQL).apply {
                setLong(1, id)
            } use { st ->
                st.executeQuery() use rs@ { rs ->
                    if (rs.next()) {
                        return@rs Account(c,  rs)
                    }

                    null
                }
            }
        }
    }

    fun get(): List<Account> {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(GET_SQL) use { st ->
                st.executeQuery() use { rs ->
                    val result = arrayListOf<Account>()

                    while (rs.next()) {
                        result.add(Account(c, rs))
                    }

                    result
                }
            }
        }
    }
}