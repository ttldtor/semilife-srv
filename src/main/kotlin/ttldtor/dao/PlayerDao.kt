package ttldtor.dao

import ttldtor.ConnectionPool
import ttldtor.entities.Account
import ttldtor.entities.Player
import ttldtor.use
import ttldtor.useWithTx
import java.sql.Connection
import java.sql.Statement

object PlayerDao {
    val CREATE_SQL = "INSERT INTO player (name, account_id, create_date, is_deleted) VALUES (?, ?, ?, 0)"
    val SAVE_SQL = "UPDATE player SET name = ? WHERE id = ?"
    val DELETE_SQL = "UPDATE player SET is_deleted = 1 WHERE id = ?"
    val GET_BY_ID_SQL = "SELECT * FROM player WHERE is_deleted = 0 AND id = ?"
    val GET_BY_ACCOUNT_ID_SQL = "SELECT * FROM player WHERE is_deleted = 0 AND account_id = ?"
    val GET_SQL = "SELECT * FROM player WHERE is_deleted = 0"

    fun create(name: String, account: Account?, createDate: Long): Player? {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(CREATE_SQL, Statement.RETURN_GENERATED_KEYS) use st@ { st ->
                st.setString(1, name)

                if (account == null) {
                    st.setNull(2, java.sql.Types.BIGINT)
                } else {
                    st.setLong(2, account.id)
                }

                st.setLong(3, createDate)

                if (st.executeUpdate() == 0) {
                    return@st null
                }

                st.generatedKeys use keys@ { keys ->
                    if (keys.next()) {
                        return@keys Player(
                                id = keys.getLong(1),
                                name = name,
                                accountId = account?.id,
                                account = account,
                                createDate = createDate)
                    }

                    null
                }
            }
        }
    }

    fun save(p: Player): Boolean {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(SAVE_SQL) use { st ->
                st.setString(1, p.name)
                st.setLong(2, p.id)

                st.executeUpdate() == 1
            }
        }
    }

    fun delete(a: Player): Boolean {
        if (a.isDeleted) {
            return true
        }

        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(DELETE_SQL) use { st ->
                st.setLong(1, a.id)

                st.executeUpdate() == 1
            }
        }
    }

    fun getById(id: Long): Player? {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(GET_BY_ID_SQL).apply {
                setLong(1, id)
            } use { st ->
                st.executeQuery() use rs@ { rs ->
                    if (rs.next()) {
                        val a = Player(
                                id = rs.getLong("id"),
                                name = rs.getString("name"),
                                accountId = rs.getLong("account_id"),
                                account = null,
                                createDate = rs.getLong("create_date"),
                                isDeleted = false)

                        return@rs a
                    }

                    null
                }
            }
        }
    }

    fun get(): List<Player> {
        return ConnectionPool.connection useWithTx { c ->
            c.prepareStatement(GET_SQL) use { st ->
                st.executeQuery() use { rs ->
                    val result = arrayListOf<Player>()

                    while (rs.next()) {
                        result.add(Player(
                                id = rs.getLong("id"),
                                name = rs.getString("name"),
                                accountId = rs.getLong("account_id"),
                                account = null,
                                createDate = rs.getLong("create_date"),
                                isDeleted = false))
                    }

                    result
                }
            }
        }
    }

    fun getByAccount(a: Account): List<Player> {
        return ConnectionPool.connection useWithTx { c ->
            getByAccount(c, a)
        }
    }

    fun getByAccount(c: Connection, a: Account): List<Player> {
        return c.prepareStatement(GET_BY_ACCOUNT_ID_SQL).apply {
            setLong(1, a.id)
        } use { st ->
            st.executeQuery() use rs@ { rs ->
                val result = arrayListOf<Player>()

                while (rs.next()) {
                    result.add(Player(
                            id = rs.getLong("id"),
                            name = rs.getString("name"),
                            accountId = a.id,
                            account = a,
                            createDate = rs.getLong("create_date"),
                            isDeleted = false))
                }

                result
            }
        }
    }


}