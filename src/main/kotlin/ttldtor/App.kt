package ttldtor

import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Liquibase
import liquibase.database.Database
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.DatabaseException
import liquibase.resource.ClassLoaderResourceAccessor
import org.slf4j.LoggerFactory

class DbUpdater {
    val log = LoggerFactory.getLogger(this.javaClass)

    fun update(): Unit {
        using {
            val c = ConnectionPool.connection.autoClose()

            c.autoCommit = false

            var db: Database? = null

            try {
                db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(c))
                val lb = Liquibase(Config.changeLogFile, ClassLoaderResourceAccessor(), db)

                lb.update(Contexts(), LabelExpression())
            } catch (e: DatabaseException) {
                log.error("Update error: ", e)
                db?.rollback()
            } finally {
                db?.close()
            }
        }
    }

}


fun main(args: Array<String>) {
    val dbUpdater = DbUpdater()

    dbUpdater.update()
}