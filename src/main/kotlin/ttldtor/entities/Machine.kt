package ttldtor.entities

class Machine(
        id: Long,
        name: String,
        val hearts: List<Heart>,
        val shields: List<Shield>,
        val weapons: List<Weapon>,
        val soul: Soul?,
        val wings: Wings?,
        val spirit: Player?,
        val master: Machine?,
        val slaves: List<Machine>): Entity(id, name)