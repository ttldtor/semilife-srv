package ttldtor.entities

class Player(
        id: Long,
        name: String,
        var accountId: Long?,
        var account: Account?,
        val createDate: Long,
        var isDeleted: Boolean = false): Entity(id, name)