package ttldtor.entities

enum class WeaponCategory {
    LASER,
    PLASMA,
    BOLT
}

data class WeaponDescription(
        val id: Long,
        val energyToLaunch: Double,
        val warmUpTime: Long,
        val warmUpBeforeLaunchTime: Long,
        val canBurstFire: Boolean,
        val numShotsInBurst: Int,
        val maxShotsNumber: Long
        )

class WeaponType(id: Long, name: String, val category: WeaponCategory, val description: WeaponDescription): Entity(id, name)