package ttldtor.entities

class Shield(id: Long, name: String, val shieldType: ShieldType, val energy: Double): Entity(id, name)