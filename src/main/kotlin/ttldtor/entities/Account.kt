package ttldtor.entities

open class Account(
        id: Long,
        login: String,
        val passwordHash: String,
        val createDate: Long,
        val players: MutableList<Player>,
        var isDeleted: Boolean = false): Entity(id, login) {
    companion object {}
    val login: String
            get() = name
}
