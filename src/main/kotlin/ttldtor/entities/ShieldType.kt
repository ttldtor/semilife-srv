package ttldtor.entities

enum class ShieldCategory {
    LASER,
    PLASMA,
    BOLT
}

data class ShieldDescription(val id: Long)

class ShieldType(
        id: Long,
        name: String,
        val shieldCategories: Set<ShieldCategory>,
        val shieldDescription: ShieldDescription): Entity(id, name)