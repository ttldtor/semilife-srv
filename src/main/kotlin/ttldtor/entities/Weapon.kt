package ttldtor.entities

class Weapon(id: Long, name: String, val weaponType: WeaponType, val energy: Double): Entity(id, name)