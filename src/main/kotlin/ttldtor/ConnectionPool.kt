package ttldtor

import org.postgresql.ds.PGPoolingDataSource
import java.sql.Connection

object ConnectionPool {
    val pool = PGPoolingDataSource().apply {
        dataSourceName = "DS"
        serverName = Config.databaseHost
        databaseName = Config.databaseName
        user = Config.databaseUser
        password = Config.databasePassword
        maxConnections = Config.databasePoolSize
    }

    val connection: Connection
        get() = pool.connection
}