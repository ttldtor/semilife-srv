package ttldtor

import com.natpryce.konfig.ConfigurationProperties
import com.natpryce.konfig.EnvironmentVariables
import com.natpryce.konfig.*
import java.io.File

object Config {
    val configFileName = "semilife-srv.properties"
    val prop = ConfigurationProperties.systemProperties() overriding
            EnvironmentVariables() overriding
//            ConfigurationProperties.fromFile(File("/$configFileName")) overriding
            ConfigurationProperties.fromResource(configFileName)

    val databaseHost: String
        get() = prop.getOrElse(Key("db.host", stringType), "127.0.0.1")

    val databaseName: String
        get() = prop.getOrElse(Key("db.name", stringType), "semilife")

    val databaseUser: String
        get() = prop.getOrElse(Key("db.user", stringType), "semilifeuser")

    val databasePassword: String
        get() = prop.getOrElse(Key("db.password", stringType), "semilifeuser")

    val databasePoolSize: Int
        get() = prop.getOrElse(Key("db.pool.size", intType), 200)

    val changeLogFile: String
        get() = prop.getOrElse(Key("db.liquibase.changeLogFile", stringType), "changelog.xml")
}